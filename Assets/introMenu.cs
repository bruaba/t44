﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class introMenu : MonoBehaviour
{
	public int compteur = 1;

    // Start is called before the first frame update
    void Start()
    {

        Debug.Log("Debug message here");
    }

    // Update is called once per frame
    void Update()
    {
    	compteur += 1;
        if(compteur == 300){
        	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        
    }

    IEnumerator Attendre(float DestroyTime){
    	yield return new WaitForSeconds(DestroyTime);
    }
}
