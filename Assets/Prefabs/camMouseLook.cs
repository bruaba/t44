﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camMouseLook : MonoBehaviour
{
    
    Vector2 mouseLook;
    Vector2 smoothy;

    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;

    GameObject character;


    // Start is called before the first frame update
    void Start()
    {
    	character = this.transform.parent.gameObject;
        
    }

    // Update is called once per frame
    void Update()
    {

    	var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

    	md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

    	smoothy.x = Mathf.Lerp(smoothy.x, md.x, 1f / smoothing);
        smoothy.y = Mathf.Lerp(smoothy.y, md.y, 1f / smoothing);
    	
    	mouseLook += smoothy;

    	transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
    	character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
    }
}
